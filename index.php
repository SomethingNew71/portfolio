<!DOCTYPE html>
<html lang="en">
  <?php include "assets/inc/header.php" ?>
  <body>
    <div id="container">     
      <div class="jumbotron jetstrap-highlighted" id="namebox">
        <img src="assets/img/mini_logo_black.png" class="">
        <h1 class="">Cole Gentry</h1>
        <p class="lead">I am a Front-End <span>Web Developer</span> providing comprehensive site construction and support. Using <span id="rotating-slogan">HTML5</span>. <a class="anim cursor" id="slogan-icon"><img src="assets/img/refresh.png" onmouseover="this.src='assets/img/refreshhov.png'" onmouseout="this.src='assets/img/refresh.png'" alt="refresh"/></a></p>
        <p>
          <a href="http://www.linkedin.com/profile/view?id=112862446&trk=tab_pro" target="_blank"><img src="assets\img\roundsocial\png\40x40\linkedin.png"></a> 
          <a href="https://www.facebook.com/SomethingNew71" target="_blank"><img src="assets\img\roundsocial\png\40x40\facebook.png"></a>
          <a href="https://twitter.com/somethingnew71" target="_blank"><img src="assets\img\roundsocial\png\40x40\twitter.png"></a>
          <a href="mailto:peapod2007@gmail.com" target="_blank"><img src="assets\img\roundsocial\png\40x40\email.png"></a>
          <!-- <a href="https://plus.google.com/107365747993894371557/posts" target="_blank"><img src="assets\img\roundsocial\png\40x40\google_plus.png"></a> -->
        </p>
      </div>
      <h2>Qualifications</h2>
      <hr />
      <div  class="row-fluid">
        <div  class="well well-small span4">
          <h3>Responsive</h3>
          <p>As a <span>Web Developer</span> my main goal is to offer a product that will last and continue to work for as long as possible. Something that will stand the test of time.  I like to create my sites <span>mobile</span> compliant to provide compatibility on all devices whether it be an iPad, iPhone, Android device or your desktop computer.</p>
        </div>
        <div class="well well-small span4">
          <h3>Design</h3>
          <p>Although focusing my skills on the the <span>front-end development</span> of a site, when designing a website I keep it minimal.  Excessive pages and links on a page are not my style nor are they condusive to a pleasent <span>user experience</span>.
          </p>
       </div>
        <div class="well well-small span4" style="display: block;">
          <h3>Languages</h3>
          <p>I am proficient in many of the major languages and services. 
            <br>
            These include but are not limited to the following:
            <ul>
              <li><span>HTML5</span></li>
              <li><span>CSS3</span></li>
              <li><span>Wordpress</span></li>
              <li><span>Javascript</span></li>
              <li><span>Git</span></li>
              <li><span>Mecurial</span></li>
            </ul>
          </p>
        </div>
      </div>
    
      
    
      <h2>Projects</h2>
      <hr />
      <div class="row-fluid" id="projects">
        <ul class="thumbnails">
          <li class="span6">
            <a href="http://www.pccmu.com" target="_blank" class="thumbnail greydout">
            <img src="assets/img/pccmu.png" alt="Piedmont Classic Minis United">
            </a>
         <!--  </li>
          <li class="span4">
            <a href="#" class="thumbnail">
            <img data-src="holder.js/300x200" alt="">
            </a>
          </li> -->
          <li class="span6">
            <a href="http://www.hayliejeterphotography.com" target="_blank" class="thumbnail greydout">
            <img src="assets/img/hjpho.png" alt="">
            </a>
          </li>
        </ul>
      </div>
      <h2>Personal Life</h2>
      <hr />
      <div class="row-fluid" >
        <ul class="thumbnails">
          <li class="span4">
            <div class="thumbnail">
              <img src="assets/img/carenthu.png" alt="picture of mini">
              <h3>Car Enthusiast</h3>
              <p>Throughout my life two things have always been a constant.  My interest in <span>Web Development</span> and my love of cars.  I have been restoring a classic mini cooper for over 3 years now.  That is my third love.  Coming after my girlfriend and my computer.</p>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail">
              <img src="assets/img/damon.png" alt="dog picture">
              <h3>Dog Lover</h3>
              <p>I am a frequent volunteer around Charlotte at various animal rescues.  Most recently we fostered a dog from <span>Great Dane Friends of Ruff Love</span>.  We ultimatly adopted the dog who's name is Damon.</p>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail">
              <img src="assets/img/minikayak.png" alt="kayak picture">
              <h3>Avid Kayaker</h3>
              <p>In the past year I have become a very regular <span>kayaker</span>.  This provides me with much needed peace and time to reflect on my own thoughts.  Going out on the Catawba river or Mountain Island lake are some of my regular places.  </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="footer">
      <p>© <span>Cole Gentry</span> 2013</p>
      </div>
    </div>
    <script src="assets/js/jquery-1.10.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/custom.js"></script>
  </body>
</html>
