$(function() {
    var sloganRefresh = [
                            "CSS3",
                            "Wordpress",
                            "PHP",                        
                            "Javascript",
                            "HTML5"
                        ];
    $('#slogan-icon').on('click', function(){
        if(!$('#slogan-icon img').hasClass('spin')){
            $('#slogan-icon img').addClass('spin');
            window.setTimeout(function() {
                
                var index = sloganRefresh.indexOf($('#rotating-slogan').html());
                if (index == sloganRefresh.length-1){
                    index = 0;
                }else{
                    index++;
                }
                $('#rotating-slogan').html(sloganRefresh[index]);
                $('#slogan-icon img').removeClass('spin');
            }, 600);
        }
    });
});
